# Capes22 Virtual Exhibit

Official repo for UP CAPES 30th: Virtual Exhibit

Download Virtual Exhibit Elements Folder [here](https://drive.google.com/drive/folders/1Uk0AcTPkMN64n8f5GQegvUOuXe8xtfnr) to include the static to your production.

## Git Documentation
Different branches to different regions that will be merged to main 
### Branches
main\
loading-screen\
welcome-screen\
space\
sky\
underwater\
deep-sea\
pearl
### Pushing changes to local branch
```
git checkout -b <branch>
git add . or specific-files
git commit -m "message"
git push -u origin <branch>
```
### Pulling changes from remote branch to local branch
```
git pull origin <branch>
```
